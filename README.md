First this is a not finalised keymap so if you have a sugerence for improve this you have two ways to do that:
1. research a little bit about how it works and modify it then make a pull recuest
2. if you really you can't do the ideas that you have into the keymap create a issue and put there your suggestions

once acclared it i will explain what is this proyect 

i'm a mexican person so my keymap is the latinoamerican spanish and i can say that is one of the worst for program so i said myself ¿how can i improve it with the linux power? and i got an answer:
I will do my own custom keymap distribution with i need for write and code. so here is.
i've used this keymap about 1 mont and i can say that for me is so comfortable for the languages that really
not use much this diacritic simbols ´ ¨ ° all other diacritic symbols are comfortables '^ `~

this languages is almost all the romance languages and some germanic languages for example the english language but not the dutch or sweden because thos languages use the  ¨  symbol too much

now we can continue on how to install it on every linux distribution at this moment does not abiable for windows or macos

so it keymap uses xkb for work so the first think to do is search in the web where are the keymap tables of xkb in our distro.
in the case of most archbase systems and ubuntu like systems will be in /usr/share/X11/xkb/
inside this folder we'll found this folders

compat
geometry
keycodes
rules
symbols
types

the folders that we have interest are symbols and rules folder.

so first we will open up a terminal

(for this step we need have instaled git, if we not have instaled git we will install it with our pakage manager)

git clone https://gitlab.com/vertecedoc_it_av_sorpio/latinoamerican-for-program.git
cd latinoamerican-for-program
cd symbols
ls -a 

at this point we are inside the folder of this repository and in the folder symbols
the command ls -a show us all the files inside this folder 

so we need to know what is your xkb keytables folder 
i will use the rute that i puted before /usr/share/X11/xkb/
BUT YOU NEED REPLACE IT FOR YOURS

now the command 

cp program-latam /usr/share/X11/xkb/symbols

this command will copy the program-latam file into the folder /usr/share/X11/xkb/symbols.
with this we have the keymap instaled but xkb (that is the program for change beteween the keymaps) doesn't know 
that we have program-latam so lets make xkb know that for this you need open the files 'add to the list.lst'  'add to the xml files.xml' inside the git repository main folder, the rute is this: latinoamerican-for-program/rules/our files

so you can open this files with your favorite editor as gedit (gnome's defaulf text editor) or kate (kde's defaulf text editor)

and copy the content of this files (first copy the content of the file 'add to the list.lst')

after this we need to put it into all files that term in .lst inside the folder /usr/share/X11/xkb/rules

so return to the terminal. the next command is:

sudo nano /usr/share/X11/xkb/rules/base.lst

it will open something like this:



![Captura de pantalla de 2021-03-21 12-12-37.png](https://gitlab.com/vertecedoc_it_av_sorpio/latinoamerican-for-program/-/raw/master/rules/images/Captura%20de%20pantalla%20de%202021-03-21%2012-12-37.png)




so now put ctl and w keys now type latam and enter should appear this:






![Captura de pantalla de 2021-03-21 12-25-49.png](https://gitlab.com/vertecedoc_it_av_sorpio/latinoamerican-for-program/-/raw/master/rules/images/Captura%20de%20pantalla%20de%202021-03-21%2012-25-49.png)




now press enter to add a new line and put over that line and now press ctl shift(mayus) and v 







![Captura de pantalla de 2021-03-21 12-31-41.png](https://gitlab.com/vertecedoc_it_av_sorpio/latinoamerican-for-program/-/raw/master/rules/images/Captura%20de%20pantalla%20de%202021-03-21%2012-31-41.png)




1we put the program-latam line so now to save it for it press ctrl and o then enter

next step press ctrl and x for quit

in the terminal put this 

sudo cp sudo nano /usr/share/X11/xkb/rules/base.lst /usr/share/X11/xkb/rules/evdev.lst

it will rewrite the evdev.lst file with the same of base.lst 

with this we got the first part maked

for the next part we need do the same but with the xml file 

with your favorite editor copy the content of the file "add to the xml files.xml"
now in the terminal
sudo nano /usr/share/X11/xkb/rules/base.xml

then press ctrl and w and tipe latam

![Captura de pantalla de 2021-03-21 13-23-35.png](https://gitlab.com/vertecedoc_it_av_sorpio/latinoamerican-for-program/-/raw/master/rules/images/Captura%20de%20pantalla%20de%202021-03-21%2013-23-35.png)

now put 2 spaces before the last \<layout\>

![Captura de pantalla de 2021-03-21 13-23-53.png](https://gitlab.com/vertecedoc_it_av_sorpio/latinoamerican-for-program/-/raw/master/rules/images/Captura%20de%20pantalla%20de%202021-03-21%2013-23-53.png)

and paste all that you copyed before with ctrl + shiht(mayus) + v

press ctrl + o then enter and finaly ctrl + x 

finally whe have finish the instalation only that we have do is go to the setting of our desktop environment and change the keyboard!!!

if you are in a window manager not in a desktop environment write this command in the terminal

localectl set-x11-keymap program-latam,program-latam pc104 basic grp:alt_shift_toggle
